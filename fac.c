#include <stdio.h>
#include <stdlib.h>

unsigned long fac(unsigned long n)
{
       if (n <= 1)
               return 1;

       else
          n = n*(fac(n-1));

       return n; 
}

int main(int argc, char** argv)
{
        unsigned long c;

        if (argc == 1)
                scanf("%lu", &c);

        else if (argc == 2)
                c = strtol(argv[1], NULL, 10);

        else
                return 1;

        printf("%lu", fac(c));
}
